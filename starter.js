// const { on } = require("nodemon");

const Apikey = "0f83ba2e4561a659b84617e8d403709e"


let apiEvent = (url) => {

    fetch(url).then((response) => 
    
        response.json().then((data) => {
    
        document.querySelector('#city').innerHTML = data.name;
        document.querySelector('#temp').innerHTML = '<i class="fas fa-temperature-low"></i> ' + data.main.temp + ' °';
        document.querySelector('#humidity').innerHTML = '<i class="fas fa-dewpoint"></i> ' + data.main.humidity + ' %';
        document.querySelector('#wind').innerHTML = '<i class="fas fa-wind"></i> ' + data.wind.speed +' km/h';
    
    })
    )
    .catch((err) => console.error('Erreur : '+ err));
    }
    
    
    const localisation = navigator.geolocation.getCurrentPosition(position => {
        
        // const { latitude, longitude } = position.coords;
        const latitude = position.coords.latitude;
        const longitude = position.coords.longitude;
        
        
        const url ="https://api.openweathermap.org/data/2.5/weather?lat=" + latitude + "&lon=" + longitude +"&units=metric&appid=" + Apikey;
    
        apiEvent(url);
        
        
      });
    
    
    
    
    
    
    document.querySelector('form').addEventListener('submit', function (e) {
        
        e.preventDefault();
        let ville = document.querySelector('#search').value;
        const url = "https://api.openweathermap.org/data/2.5/weather?q="+ ville + "&lang=fr&units=metric&appid=" + Apikey ;
    
    
        apiEvent(url);
    
        // document.querySelector('#search').value = '';
    
        
    
    
    });
    
    
    
    
    const autoCompleteJS = new autoComplete({
        selector: "#search",
        placeHolder: "Rechercher une ville...",
        data: {
            src: async (query) => {
              try {
                // Fetch Data from external Source
                const source = await fetch(`https://geo.api.gouv.fr/communes?nom=${query}&boost=population&limit=10`);
                // Data is array of `Objects` | `Strings`
                const data = await source.json();
    
    
                // Avec un foreach
                // let items = [];
                // data.forEach(row => {
                //   items.push({id: row.code, text: row.nom});
                // });
                // return data;
    
                // Ou alors avec map 
                return data.map(row => { return row.nom })
    
    
              } catch (error) {
                return error;
              }
            },
            cache: false,
        },
        resultsList: {
            element: (list, data) => {
                if (!data.results.length) {
                    // Create "No Results" message element
                    const message = document.createElement("div");
                    // Add class to the created element
                    message.setAttribute("class", "no_result");
                    // Add message text content
                    message.innerHTML = `<span>Aucun résultat pour "${data.query}"</span>`;
                    // Append message element to the results list
                    list.prepend(message);
                }
            },
            noResults: true,
        },
        resultItem: {
            highlight: true
        },
        events: {
            input: {
                selection: (event) => {
                    const selection = event.detail.selection.value;
                    autoCompleteJS.input.value = selection;
                }
            }
        }
    });
    
    
    // document.querySelector("#search").addEventListener("selection", function (event) {
        // "event.detail" carries the returned data values
        // console.log('Search meteo for city : ' + event.detail.selection.value);
    // });
    
    